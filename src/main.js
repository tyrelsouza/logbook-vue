import Vue from 'vue'
import Axios from 'axios'
import App from './App.vue'
import router from './router'
import store from './plugins/store'
import moment from 'moment'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUser, faUserFriends, faCloudMoon, faGamepad, faPlaneDeparture, faPlaneArrival, faStopwatch} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUser)
library.add(faUserFriends)
library.add(faCloudMoon)
library.add(faPlaneDeparture)
library.add(faPlaneArrival)
library.add(faStopwatch)
library.add(faGamepad)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false
Vue.prototype.moment = moment


Vue.prototype.$http = Axios;
const token = localStorage.getItem('token')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = `Bearer ${token}`
  Vue.prototype.$http.defaults.headers.common['Content-Type'] =  'application/json'
}

Vue.use(store)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

