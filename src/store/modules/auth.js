import axios from 'axios'
import Vue from 'vue'

// Make Axios play nice with Django CSRF
axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

export const SET_AUTH_USER = 'setAuthUser'
export const UPDATE_TOKEN = 'updateToken'
export const REMOVE_TOKEN = 'removeToken'

const state = function () {
  return {
    authUser: {},
    isAuthenticated: false,
    jwt: localStorage.getItem('token'),
    endpointObtainJWT: 'http://127.0.0.1:8001/api/token/',
    endpointRefreshJWT: 'http://127.0.0.1:8001/api/refresh/',
    endpointBaseURL: 'http://127.0.0.1:8001/api/v1/'
  }
}

const mutations = {
  [SET_AUTH_USER] (state, { authUser, isAuthenticated }) {
    Vue.set(state, 'authUser', authUser)
    Vue.set(state, 'isAuthenticated', isAuthenticated)
  },
  [UPDATE_TOKEN] (state, newToken) {
    // TODO: For security purposes, take localStorage out of the project.
    localStorage.setItem('token', newToken);
    state.jwt = newToken;
  },
  // [REMOVE_TOKEN] (state) {
  //   // TODO: For security purposes, take localStorage out of the project.
  //   localStorage.removeItem('token');
  //   state.jwt = null;
  // }
}

const getters = {
  obtainJWT: (state) => {
    return state.endpointObtainJWT
  },
  refreshJWT: (state) => {
    return state.endpointRefreshJWT
  },
  baseURL: (state) => {
    return state.endpointBaseURL
  },
  jwt: (state) => {
    return state.jwt
  },
  isAuthenticated: (state) => {
    return state.isAuthenticated
  },
  authUser: (state) => {
    return state.authUser
  },

}

const actions = {
  async setAuthUser({ commit }, {authUser, isAuthenticated}) {
    commit(SET_AUTH_USER, { authUser, isAuthenticated})
  },
  async updateToken({ commit }, token) {
    commit(UPDATE_TOKEN, token)
  },
  authenticate ({getters, dispatch}, {username, password}) {
    const payload = {
      username: username,
      password: password
    }
    axios.post(getters.obtainJWT, payload)
      .then((response) => {
        dispatch('updateToken', response.data.access)
        const base = {
          baseURL: getters.baseURL,
          headers: {
            Authorization: `Bearer ${getters.jwt}`,
            'Content-Type': 'application/json'
          },
          xhrFields: {
              withCredentials: true
          }
        }
        // Even though the authentication returned a user object that can be
        // decoded, we fetch it again. This way we aren't super dependant on
        // JWT and can plug in something else.
        const axiosInstance = axios.create(base)
        axiosInstance({
          url: "/users/1/", // TODO DONT FUCKING HARDCODE THIS YOU BOZO
          method: "get",
          params: {}
        })
        .then((response) => {
          dispatch('setAuthUser', {authUser: response.data, isAuthenticated: true})
        })
      })
      .catch((error) => {
        console.log(error);
        console.debug(error);
        console.dir(error);
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}