import _ from 'lodash'
import axios from 'axios'

export const LOG_ENTRIES = 'logEntries'

// initial state
const state = function () {
  return {
    logEntries: []
  }
}

const getters = {
  logEntries: (state) => {
    return state.logEntries
  },
  entriesByDate (state)  {
    return _.groupBy(state.logEntries, (n) => {
        return n.flight_date
    })
  }
}

const actions = {
  addEntry ({ commit, state }, entry) {
    if (entry === undefined) return
    commit(LOG_ENTRIES, [...state.logEntries, entry])
  },
  async downloadEntries({commit, state, rootState}){
    const base = {
        baseURL: rootState.auth.endpointBaseURL,
        xhrFields: {
            withCredentials: true
        }
      }
      const axiosInstance = axios.create(base)
      axiosInstance({
        url: "/flights/",
        method: "get",
        params: {}
      })
      .then((response) => {
          response.data.results.forEach(flight => {
            commit(LOG_ENTRIES, flight)
          });
      })
  }
}

const mutations = {
  [LOG_ENTRIES] (state, entry) {
    state.logEntries.push(entry)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}