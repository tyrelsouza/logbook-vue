import Vue from 'vue'
import Vuex from 'vuex'
import path from 'path'
Vue.use(Vuex)

const allModules = require.context('./modules', true, /\.js$/)

const modules = allModules.keys().reduce((all, key) => ({
  ...all, [path.basename(key, '.js')]: allModules(key).default
}), {})

const debug = process.env.NODE_ENV !== 'production'

var auth = modules.auth

export default new Vuex.Store({
    modules,
    strict: debug,
  })